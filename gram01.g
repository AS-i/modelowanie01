grammar gram01;

@members{ 
class ZeroDiv extends RuntimeException  { 
	ZeroDiv() {
			System.out.println("ZERO DIV");
		}
	}
}

  plik 
  	:	(expr {System.out.println("Wynik: "+ $expr.wyj);}NL+)* EOF
  	;
  	
  expr returns [Integer wyj]
  	:	term1=term{$wyj = $term1.wyj;}
  		 ((PLUS term2=term {$wyj += $term2.wyj;} | MINUS term2=term{$wyj -= $term2.wyj;})) * NL*
            ;
                        
  term returns [Integer wyj] 
  	:	atom1=atom{$wyj = $atom1.wyj;} 
  		((MUL atom2=atom{$wyj *= $atom2.wyj;} 
  		| DIV atom2=atom{if($atom2.wyj == 0 ) throw new ZeroDiv(); $wyj /= $atom2.wyj;}
  		| MOD atom2=atom{if($atom2.wyj == 0 ) throw new ZeroDiv(); $wyj = wyj \% $atom2.wyj;} )) *   
            ;
                        
  atom returns [Integer wyj]
  	:	INT {$wyj = Integer.parseInt($INT.text);}
  		|  (LP expr1= expr RP) {{$wyj = $expr1.wyj;}}
  	;


INT :	('0'..'9')+ ;

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;};
  
WS  :   ( ' '
        | '\t'
        | '\r'
        ) {$channel=HIDDEN;}; 
         
DIV		:'/';
MUL		:'*';
MOD		:'%';
PLUS		:'+';
MINUS	: '-';
NL		: '\n';

LP 	:	'(';
RP 	:	')';
